/**
* @author : Emil
* ID : C3271566
* Last Edited : 13th May 2019
* Purpose : Main program
*/

/**
* TODO implement decryption method
* write output file
* running time
* different results for different AES
* flip bits
*/

import java.io.*;
import java.util.*;
import java.math.BigInteger;

public class Application {

  private final int BIT_LENGTH = 128;
  private State statePlainText;
  private State stateCipherText;
  private State stateKey;
  private State stateKeyBeforeExpansion;
  private String key;
  private boolean isDecrypt;
  private String[][] matrix = {
	  {"02","03","01","01"},
    {"01","02","03","01"},
    {"01","01","02","03"},
    {"03","01","01","02"}
  }; //matrix for mix columns
  private String[][] invMatrix = {
	{"0e", "0b", "0d", "09"},
	{"09", "0e", "0b", "0d"},
	{"0d", "09", "0e", "0b"},
	{"0b", "0d", "09", "0e"}
  }; //matrix for inverse mix columns
  private String[][] rcon = {
    { "01", "00", "00", "00" },
	  { "02", "00", "00", "00" },
	  { "04", "00", "00", "00" },
	  { "08", "00", "00", "00" },
	  { "10", "00", "00", "00" },
	  { "20", "00", "00", "00" },
	  { "40", "00", "00", "00" },
	  { "80", "00", "00", "00" },
	  { "1b", "00", "00", "00" },
	  { "36", "00", "00", "00" }
  }; //rcon to be used in the key schedule

  public static void main (String[] args){
    Application sim = new Application(args);
    sim.run();
  }
  public Application (String[] args){

    String fileName = ""; //file name
    String line; //read lines from the file

    if (args.length < 3){
      //Alert error if there is command line format is wrong
      System.out.println("\nCommand line format : java Application --encrypt(decrypt) <input filename> <output filename>\n");
      System.exit(0);
    }
  	if (args[0].equals("--decrypt")){ isDecrypt = true; }
  	else if (args[0].equals("--encrypt")){ isDecrypt = false; }
  	else {
  		//Alert error if not specified
  		System.out.println("\nOnly <--encrypt> and <--decrypt> are allowed.\n");
  		System.exit(0);
  	}

    fileName = args[1];
    //Open file
    try {
        /******************************DELETE THE BLOCK BELOW BEFORE SUBMITTING THE ASSIGNMENT*******************************/
        // Scanner inputStream = new Scanner(new FileReader(fileName));
        // String plainText = inputStream.nextLine(); //Read first line as key
        // statePlainText = new State(plainText); //Store the line in the form of state
        // String key = inputStream.nextLine(); //Read second line as key
        // stateKey = new State(key); //Store the line in the form of state

      // //TODO uncomment the following block of text
      Scanner inputStream    = new Scanner(new FileReader(fileName));
  	  if (isDecrypt){
  		  String binCipherTextStr  = inputStream.nextLine(); //Read first line as binary cipherText
  		  // while (binCipherTextStr.length() < BIT_LENGTH){ binCipherTextStr += "0"; }
  		  //Reading the plaintext
  		  BigInteger binCipherText = new BigInteger(binCipherTextStr, 2);
  		  String cipherText        = binCipherText.toString(16);
  		  stateCipherText          = new State(cipherText);
  	  }
  	  else {
        //Reading the plaintext
  		  String binPlainTextStr = inputStream.nextLine(); //Read first line as binary plaintext
  		  String plainText       = binToHex(binPlainTextStr);
  		  statePlainText         = new State(plainText);
  	  }
      //Reading the key
      String binKeyStr = inputStream.nextLine(); //Read second line as binary key
      key       = binToHex(binKeyStr);
      stateKey         = new State(key);
      stateKeyBeforeExpansion = new State(key);
    }
    catch (FileNotFoundException e){
      //alert error if file is not found
      System.out.println("\nNO SUCH FILE EXISTS\n");
      System.exit(0);
    }
  }
  public void run(){
    // System.out.println(hexToBin(statePlainText));
    statePlainText.print();
    // System.out.println(getString());
    stateKey.print();
    
    AES0();
    // System.out.println(hexToBin(stateKey));
    stateCipherText.print();
    System.out.println(hexToBin(stateCipherText));

    stateKey         = new State(key);
    System.out.println("Now decrypting");
    invAES0();
    statePlainText.print();
    System.out.println(hexToBin(statePlainText));
    // System.out.println(stateCipherText.toBin());
  	AES1();
  	AES2();
  	AES3();
    AES4();
  }
  public String binToHex (String bin){
    String[] splitStr = bin.split("(?<=\\G........)");
    String str = "";
    for (int i = 0; i < splitStr.length; i++){
      int temp = Integer.parseInt(splitStr[i], 2);
      str += String.format("%02x", temp);
    }
    return str;
  }
  public String hexToBin (State hex){
    String str = "";
    for (int j = 0; j < 4; j++){
      for (int i = 0; i < 4; i++){
        String temp = Integer.toBinaryString(
                      Integer.parseInt(hex.getBox(i, j), 16)
                      );
        String formatPad = "%"+(8)+"s";
        str += String.format(formatPad, temp).replace(" ", "0");
      }
    }
    return str;
  }
  public void displayOutput(){

    /**
    * TODO uncomment the block below once done with the assignment
    * Displaying the output of the program
    * as required in the assignment specifications
    */

    // System.out.println();
    // System.out.println("ENCRYPTION");
    // System.out.println("Plaintext P: "+plainText);
    // System.out.println("Key K: "+key);
    // System.out.println("Ciphertext C: "+cipherText);
    // System.out.println("Running time: "+runningTime);
    // System.out.println("Avalanche: ");
    // System.out.println("P and Pi under K");
  }
  public void AES0(){
    //Round 0
    statePlainText.addRoundKey(stateKey);
    System.out.println("Round 0 after add round key :");
    statePlainText.print();
    //Round 1 to Round 9 standard operations
    for (int round = 1; round <= 10; round++){
  		statePlainText.subBytes();
  		statePlainText.shiftRows();
  		if (round < 10){ statePlainText.mixColumns(matrix);
	      }
  		stateKey.genRoundKey(rcon[round - 1]);
  		statePlainText.addRoundKey(stateKey);
      statePlainText.print();

  	}
    stateCipherText = statePlainText;
  }
  public void invAES0() {
	  stateKey.genRoundKey(rcon[9]);
	  stateCipherText.addRoundKey(stateKey);
	  for (int round = 9; round >= 1; round--) {
		  stateCipherText.invShiftRows();
		  stateCipherText.invSubBytes();
		  stateKey.genRoundKey(rcon[round-1]);
		  stateCipherText.addRoundKey(stateKey);
		  stateCipherText.invMixColumns(invMatrix);
	  }
	  stateCipherText.invShiftRows();
	  stateCipherText.invSubBytes();
	  stateCipherText.addRoundKey(stateKeyBeforeExpansion);
	  statePlainText = stateCipherText;
  }
  public String getString(){
    String str = "";
    for (int j = 0; j < 4; j++){
      for (int i = 0; i < 4; i++){
        str += statePlainText.getBox(i, j) ;
      }
    }
    return str;
  }
  public void AES1(){
	  statePlainText.addRoundKey(stateKey);
	  for (int round = 1; round <= 10; round++){
		  statePlainText.shiftRows();
		  if (round < 10){ statePlainText.mixColumns(matrix); }
		  stateKey.genRoundKey(rcon[round - 1]);
		  statePlainText.addRoundKey(stateKey);
	  }
  }
  public void AES2(){
	  statePlainText.addRoundKey(stateKey);
	  for (int round = 1; round <= 10; round++){
		  statePlainText.subBytes();
		  if (round < 10){ statePlainText.mixColumns(matrix); }
		  stateKey.genRoundKey(rcon[round - 1]);
		  statePlainText.addRoundKey(stateKey);
	  }
  }
  public void AES3(){
	  statePlainText.addRoundKey(stateKey);
	  for (int round = 1; round <= 10; round++){
		  statePlainText.subBytes();
		  statePlainText.shiftRows();
		  stateKey.genRoundKey(rcon[round - 1]);
		  statePlainText.addRoundKey(stateKey);
	  }
  }
  public void AES4(){
	  statePlainText.addRoundKey(stateKey);
	  for (int round = 1; round <= 10; round++){
		  statePlainText.subBytes();
		  statePlainText.shiftRows();
		  if (round < 10){ statePlainText.mixColumns(matrix); }
	  }
    stateCipherText = statePlainText;
  }
}
