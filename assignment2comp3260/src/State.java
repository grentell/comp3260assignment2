/**
* @author : Emil
* ID : C3271566
* Last Edited : 10th May 2019
* Purpose : declaring state object
*/

//TODO add decryption to the methods
public class State {
  //Private variables
  private final int KEY_LENGTH  = 32; //Assuming hexadecimal in 128 bit
  private final int MAX_ROWS    = 4;
  private final int MAX_COLUMNS = 4;
  private String box[][]        = new String[MAX_ROWS][MAX_COLUMNS];
  private Sbox sbox             = new Sbox(); //Sbox for bytes substitutions
  
  //Default constructor
  public State (){}
  //Custom constructor
  public State (String str){
    String[] splitStr = str.split("(?<=\\G..)");
    int index = 0;
    //insert every 2 characters in the string using for loop
    for (int j = 0; j < MAX_COLUMNS; j++){
      for (int i = 0; i < MAX_ROWS; i++){
        box[i][j] = splitStr[index];
        index++;
      }
    }
  }
  //Functions
  //TODO delete the method below
  public void print(){
    for (int i = 0; i < MAX_ROWS; i++){
      for (int j = 0; j < MAX_COLUMNS; j++){
        System.out.print(box[i][j]+" ");
      }
      System.out.println();
    }
    System.out.println();
  }
  //TODO delete the following method
  public void print(String[] arr){
    for (int i = 0; i < arr.length; i++){
      System.out.println(arr[i]);
    }
    System.out.println();
  }
  //Add round key algorithm in AES
  public void addRoundKey(State key){
    for (int i = 0; i < MAX_ROWS; i++){
      for (int j = 0; j < MAX_COLUMNS; j++){
        //XOR operations
        int hex     = Integer.parseInt(box[i][j], 16); //Convert hex string to decimal
        int hex2    = Integer.parseInt(key.getBox(i,j), 16); //Convert hex string to decimal
        int temp    = hex ^ hex2; //XOR
        box[i][j]   = String.format("%02x", temp); //format string with leading zero
      }
    }
  }
  //bytes substitutions algorithm in AES
  public void subBytes(){
    String pos; //Coordinates of the content retrieved on the table
    String temp; //Content retrieved from the table
    for (int i = 0; i < MAX_ROWS; i++){
      for (int j = 0; j < MAX_COLUMNS; j++){
        pos       = box[i][j];
        temp      = sbox.getBox(pos);
        box[i][j] = temp;
      }
    }
  }
  public void invSubBytes() {
	String pos; //Coordinates of the content retrieved on the table
    String temp; //Content retrieved from the table
    for (int i = 0; i < MAX_ROWS; i++){
      for (int j = 0; j < MAX_COLUMNS; j++){
        pos       = box[i][j];
        temp      = sbox.getInvBox(pos);
        box[i][j] = temp;
      }
    }
  }
  //rows shifting algorithm in AES
  public void shiftRows(){
    for (int i = 0; i < MAX_ROWS; i++){
      for (int times = 0; times < i; times++){
        int j = 0;
        String temp = box[i][j];
        while (j+1 < MAX_COLUMNS){ //shifting
          box[i][j] = box[i][j+1];
          j++;
        }
        box[i][MAX_COLUMNS-1] = temp;
      }
    }
  }
  public void invShiftRows() {
	for (int i = 0; i < MAX_ROWS; i++){
      for (int times = 0; times < i; times++){
        int j = MAX_COLUMNS-1;
        String temp = box[i][j];
        while (j-1 > 0){ //shifting
          box[i][j] = box[i][j-1];
          j--;
        }
        box[i][0] = temp;
      }
    }
  }
  //mix columns algorithm in AES
  public void mixColumns(String[][] matrix){
    String results[] = new String[MAX_ROWS]; //results from XOR operation of the corresponding column
    int temp[] = new int[MAX_COLUMNS]; //temporary result of multiplication
    int result; //temporary result from XOR operation of a SINGLE box
    for (int j = 0; j < MAX_COLUMNS; j++){
      for (int i = 0; i < MAX_ROWS; i++){
        for (int j2 = 0; j2 < MAX_COLUMNS; j2++){
          temp[j2] = product(box[j2][j], matrix[i][j2]);
        }
        result = temp[0] ^ temp[1] ^ temp[2] ^ temp[3];
        results[i] = String.format("%02x", result);
      }
      for (int i = 0; i < MAX_ROWS; i++){ box[i][j] = results[i]; }
    }
  }
//mix columns algorithm in AES
  public void invMixColumns(String[][] matrix){
    String results[] = new String[MAX_ROWS]; //results from XOR operation of the corresponding column
    int temp[] = new int[MAX_COLUMNS]; //temporary result of multiplication
    int result; //temporary result from XOR operation of a SINGLE box
    for (int j = 0; j < MAX_COLUMNS; j++){
      for (int i = 0; i < MAX_ROWS; i++){
        for (int j2 = 0; j2 < MAX_COLUMNS; j2++){
          temp[j2] = product(box[j2][j], matrix[i][j2]);
        }
        result = temp[0] ^ temp[1] ^ temp[2] ^ temp[3];
        results[i] = String.format("%02x", result);
      }
      for (int i = 0; i < MAX_ROWS; i++){ box[i][j] = results[i]; }
    }
  }
  //generate round key according to the key schedule
  public void genRoundKey(String[] rcon){
    //Take out the last column of the current round key
    String[] arr = new String[MAX_ROWS];
    for (int i = 0; i < MAX_ROWS; i++){
      arr[i] = box[i][MAX_COLUMNS-1];
    }
    //Do the subBytes
    for (int i = 0; i < MAX_ROWS; i++){
      arr[i] = sbox.getBox(arr[i]);
    }
    //Do the shifting
    String temp = arr[0];
    for (int i = 0; i < (MAX_ROWS-1); i++){
      arr[i] = arr[i+1];
    }
    arr[MAX_ROWS-1] = temp;
  	//Do the XOR
  	for (int i = 0; i < MAX_ROWS; i++){
      //converting the required parameters from hex string to decimal
  		int a = Integer.parseInt(box[i][0], 16);
  		int b = Integer.parseInt(arr[i], 16);
  		int c = Integer.parseInt(rcon[i], 16);
      int d = c ^ b;
  		arr[i] = String.format("%02x", (a ^ d));
  	}
  	//generate new roundkey
  	for (int i = 0; i < MAX_ROWS; i++){
  		box[i][0] = arr[i];
  	}
  	for (int j = 1; j < MAX_COLUMNS; j++){
  		for (int i = 0; i < MAX_ROWS; i++){
        //converting the required parameters from hex string to decimal
  			int a = Integer.parseInt(box[i][j], 16);
  			int b = Integer.parseInt(box[i][j-1], 16);
  			box[i][j] = String.format("%02x", (a ^ b));
  		}
  	}
  }
  //Multiplication method used in Galois Field
  public int product(String hexA, String hexB) {
    int a = Integer.parseInt(hexA, 16);
    int b = Integer.parseInt(hexB, 16);
    int c = 0;
    for (int i = 0; i < 8; i++) {
      c = ((b & 0x01) > 0) ? c ^ a : c;
      boolean ho = ((a & 0x80) > 0);
      a = ((a << 1) & 0xFE);
      if (ho) { a = a ^ 0x1b; }
      b = ((b >> 1) & 0x7F);
    }
    return c;
  }
  public String toBin(){
    String str = "";
    for (int i = 0; i < MAX_COLUMNS; i++){
      for (int j = 0; j < MAX_ROWS; j++){
        String temp = Integer.toBinaryString(
                      Integer.parseInt(box[i][j], 16)
                      );
        String formatPad = "%"+(8)+"s";
        str += String.format(formatPad, temp).replace(" ", "0");
        // String temp = new BigInteger(box[i][j], 16).toString(2);
        // // String formatPad = "%" + (box[i][j].length() * 4) + "s";
        // // str += String.format(formatPad, temp).replace(" ", "0");
        // str += temp;
      }
    }
    // String a = "05";
    // int temp1 = Integer.parseInt(a, 16);
    // String temp = Integer.toBinaryString(temp1);
    // // System.out.println(temp);
    // String format = "%"+(8)+"s";
    // System.out.println(String.format(format, temp).replace(" ", "0"));
    return str;
  }
  //Setters
  public void setBox(int i, int j, String box){ this.box[i][j] = box; }
  //Getters
  public String getBox(int i, int j){ return box[i][j]; }
}
